"""Application exporter Moon Phase"""

import os
import time
import signal
import sys
from prometheus_client import start_http_server, Gauge
from moon import Moon



class AppMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, polling_interval_seconds=5):
        self.polling_interval_seconds = polling_interval_seconds

        # Prometheus metric`s to collect
        self.moon_phase_percent = Gauge("app_moon_phase_percent", "Moon Phase in percents")
        self.request_time_ms = Gauge("app_request_time_ms", "Request time in ms")

    def run_metrics_loop(self, app):
        """Metrics fetching loop"""

        while True:
            self.fetch(app)
            time.sleep(self.polling_interval_seconds)

    def fetch(self, app):
        """
        Get metrics from application and refresh Prometheus metrics with
        new values.
        """
        try:
            status_data = app.get_moon_phase()
            # Update Prometheus metrics with application metrics
            self.moon_phase_percent.set(status_data["moon_phase_percent"])
            self.request_time_ms.set(status_data["request_time_ms"])
        except KeyError:
            pass

def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    sys.exit(0)

def main():
    signal.signal(signal.SIGTERM, sigterm_handler)

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "500"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9100"))
    try:
        moon = Moon(os.getenv("WEATHER_API_LOCATION_CITY", "kyiv"), os.getenv("WEATHER_API_TOKEN"))

        app_metrics = AppMetrics(
            polling_interval_seconds=polling_interval_seconds
        )
        start_http_server(exporter_port)
        app_metrics.run_metrics_loop(moon)
    except Exception:
        pass


if __name__ == "__main__":
    main()
