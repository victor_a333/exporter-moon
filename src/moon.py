# Class uses visualcrossing.com to get moon phase
# https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/kyiv/today?unitGroup=metric&elements=moonphase&include=current&key=VGHHRVGVP5T42FP6E8JE9N8BZ&contentType=json

import requests
import logging
import time
import sys
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class Moon:
    def __init__(self, location, api_token):
        if location is None:
            logger.error("Weather Api location must be set")
            sys.exit(1)
        if api_token is None:
            logger.error("Weather Api token must be set")
            sys.exit(1)
        self.api_token = api_token
        self.location = location

    def _get_weather_api_response(self):
        resp = requests.get(url=f"https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{self.location}/today?unitGroup=metric&elements=moonphase&include=current&key={self.api_token}&contentType=json")
        if resp.status_code == 200:
            return resp
        else:
            logger.error(resp.text)
            # Wrong location cat give response code 400 and message "Bad API Request:Invalid location parameter value."
            if resp.text.startswith("Bad API Request") or resp.text.startswith("No account found with API key"):
                sys.exit(1)

  
    def _convert_moon_phase_to_percent(self, moon_phase):
        # weather.visualcrossing.com returns moon phase in range 0 .. 1
        return int(moon_phase * 100)
    
    def get_moon_phase(self):
        moon_phase = {}
        try:
            weather = self._get_weather_api_response()
            moon_phase["request_time_ms"] = int(weather.elapsed.total_seconds() * 1000)
            moon_phase_to_percent = self._convert_moon_phase_to_percent(weather.json()["days"][0]["moonphase"])
            moon_phase["moon_phase_percent"] = moon_phase_to_percent
        except SystemExit:
            raise
        except Exception as e:
            logger.error(f"error getting Moon phase: {e}")
        else:
            logger.info(time.strftime('%m/%d/%Y, %H:%M:%S: ') + str(moon_phase))

        return moon_phase
        
