# Moon Phase prometheus exporter

## Description

     Expose in prometheus current moon phase in percents(0 - Start new phase, 100% - end phase)

## Exporting metrics

     - app_moon_phase_percent
     - app_request_time_ms

## Application uses <https://visualcrossing.com> for fetching Moon state

For fetching result You must register there and set environment variable 'WEATHER_API_TOKEN'

## Using in Kubernetes

 You can use application in Kubernetes. See example moonphase.yaml

Prometheus automatically will fetch metrics from new Pods with kubernetes_sd_configs <https://prometheus.io/docs/prometheus/latest/configuration/configuration/#kubernetes_sd_config>
If You have different configuration You must set up Prometheus for fetching data from exporter.

## Environment variables

    - POLLING_INTERVAL_SECONDS - Specify time how ofter fitch Moon phase state from weather site
    - EXPORTER_PORT - Port when application listens for prometheus requests
    - WEATHER_API_LOCATION_CITY - Specify your city. Default "kyiv"
    - WEATHER_API_TOKEN - Security token from visualcrossing.com  user account
